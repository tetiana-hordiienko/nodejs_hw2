const daoUser = require('../models/dao/daoUser');
const bcrypt = require('bcrypt');
const {DatabaseError, BadRequestError} = require('../models/errorModel');
const {User} = require('../models/userModel');

module.exports.getProfileInfo = async (req, res) => {
  const _id = req.user['_id'];
  const {username, createdDate} = await daoUser.findUserById(_id);
  res.json({user: {_id, username, createdDate}});
};

module.exports.deleteProfile = async (req, res) => {
  const _id = req.user['_id'];
  const {username} = await daoUser.findUserById(_id);
  await daoUser.deleteUserByUsername(username);
  res.json({message: 'Profile deleted successfully!'});
};

module.exports.changeProfilePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  const _id = req.user['_id'];
  const user = await daoUser.findUserById(_id);

  if (!(await bcrypt.compare(oldPassword, user.password))) {
    throw new BadRequestError('Wrong password!');
  }
  const hashed = await bcrypt.hash(newPassword, 10);
  await User.findById(
      _id,
      {password: hashed},
      (err, user) => {
        if (err) {
          throw new DatabaseError('Update failed!');
        }
        user.password = hashed;
        user.save();
      },
  );
  res.json({message: 'Password changed successfully!'});
};

