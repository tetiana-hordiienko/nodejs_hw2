const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const config = require('config');
const JWT_SECRET = config.get('JWT_SECRET');

const {User} = require('../models/userModel');
const {BadRequestError} = require('../models/errorModel');
const daoUser = require('../models/dao/daoUser');

module.exports.register = async (req, res) => {
  const {username, password} = req.body;

  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });
  await user.save();
  res.json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;
  const user = await daoUser.findUserByName(username);
  if (!(await bcrypt.compare(password, user.password))) {
    throw new BadRequestError('Wrong password!');
  }

  const token = jwt.sign(
      {username, _id: user._id},
      JWT_SECRET,
  );

  res.json({message: 'Success', jwt_token: token});
};
