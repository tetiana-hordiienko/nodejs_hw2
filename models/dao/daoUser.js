const {User} = require('../userModel');
const {BadRequestError} = require('../../models/errorModel');
const {userExists} = require('../../routers/helpers/checksHelper');

module.exports.findUserById = async (_id) => {
  if (!(await userExists({_id}))) {
    throw new BadRequestError('No user found');
  }
  return (await User.findById(_id));
};

module.exports.findUserByName = async (username) => {
  if (!(await userExists({username}))) {
    throw new BadRequestError('No user found');
  }
  return (await User.findOne({username}));
};

module.exports.deleteUserByUsername = async (username) => {
  await User.deleteOne({username});
};

module.exports.checkCandidate = async (req, res, next) => {
  const {username} = req.body;
  if (await userExists({username})) {
    throw new BadRequestError(`User ${username} already exists!`);
  }
  next();
};
