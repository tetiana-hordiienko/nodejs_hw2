const {Note} = require('../noteModel');
const {
  UnathorizedError,
  BadRequestError,
  DatabaseError,
} = require('../errorModel');
const {ensureNoteAccess} = require('../../routers/helpers/checksHelper');

const findAllNotes = async (userId, options) => {
  const {
    offset = 0,
    limits = 15,
    sortField = 'createdDate',
    sortDirection = 'asc',
  } = options;
  const requestOptions = {
    skip: parseInt(offset),
    limit: limits > 100 ? 15 : parseInt(limits),
    sort: {},
  };
  requestOptions.sort[sortField] = sortDirection === 'asc' ? 1 : -1;
  try {
    return (await Note.find(
        {userId},
        ['-__v'],
        requestOptions,
    ));
  } catch (e) {
    new DatabaseError('Can\'t read DB');
  }
};

const findNoteById = async (noteId, userId) => {
  const note = await Note.findById(noteId).exec();
  if (!note) {
    throw new BadRequestError('Note not found!');
  }
  if (!ensureNoteAccess(userId, note.userId)) {
    throw new UnathorizedError('Not allowed access this note');
  }
  return note;
};

const addNote = async (id, text) => {
  const note = new Note({
    userId: id,
    text,
  });

  try {
    await note.save();
  } catch (e) {
    new DatabaseError('Failed saving note to DB');
  }
};

const removeNote = async (noteId, userId) => {
  await findNoteById(noteId, userId);
  try {
    await Note.findByIdAndDelete({_id: noteId});
  } catch (e) {
    new DatabaseError('Note delete failed');
  }
};

const updateNote = async (note, fieldName, newValue) => {
  const {_id} = note;
  await Note.findById(
      _id,
      {fieldName: newValue},
      (err, note) => {
        if (err) {
          throw new DatabaseError('Update failed!');
        }
        note[fieldName] = newValue;
        note.save();
      },
  );
};

// const editNote = async (_id) => {
//
// };

module.exports = {
  findAllNotes,
  findNoteById,
  addNote,
  removeNote,
  updateNote,
  // editNote,
};
