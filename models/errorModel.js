// eslint-disable-next-line require-jsdoc
class ApplicationError extends Error {
  // eslint-disable-next-line require-jsdoc
  get name() {
    return this.constructor.name;
  }
}

// eslint-disable-next-line require-jsdoc
class BadRequestError extends ApplicationError {
  // eslint-disable-next-line require-jsdoc
  constructor(message = 'Bad Request') {
    super(message);
    this.statusCode = 400;
    // this.name = 'BadRequestError';
  }
}

// eslint-disable-next-line require-jsdoc
class UnathorizedError extends ApplicationError {
  // eslint-disable-next-line require-jsdoc
  constructor(message = 'Unathorized user') {
    super(message);
    this.statusCode = 401;
    // this.name = 'UnathorizedError';
  }
}

// eslint-disable-next-line require-jsdoc
class DatabaseError extends BadRequestError {
  // eslint-disable-next-line require-jsdoc
  constructor(message = 'Database Request Error') {
    super(message);
  }
}

module.exports = {
  BadRequestError,
  UnathorizedError,
  DatabaseError,
};
