const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const config = require('config');
const PORT = process.env.PORT ||config.get('PORT') || 8080;
const MONGO_URI = config.get('MONGO_URI');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const noteRouter = require('./routers/noteRouter');
const {errorHandler} = require('./controllers/errorController');

const app = express();
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users/me', userRouter);
app.use('/api/notes', noteRouter);

app.use(errorHandler);

const start = async () => {
  await mongoose.connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, () => {
    console.log(`Server started on port ${PORT}`);
  });
};

start();
