const express = require('express');
const router = new express.Router();

const {asyncWrapper} = require('./helpers/asyncHelper');
const {authMiddleware} = require('./middlewares/authMiddleware');
const {validateCreationNote} = require('./middlewares/validationMiddleware');
const {
  getUserNotes,
  addUserNotes,
  getUserNoteById,
  deleteUserNoteById,
  toggleCompletedForUserNoteById,
  updateUserNoteById,
} = require('../controllers/noteController');

router.use(asyncWrapper(authMiddleware));

router.get('/', asyncWrapper(getUserNotes));
router.post(
    '/',
    asyncWrapper(validateCreationNote),
    asyncWrapper(addUserNotes),
);
router.get('/:id', asyncWrapper(getUserNoteById));
router.put('/:id', asyncWrapper(updateUserNoteById));
router.patch('/:id', asyncWrapper(toggleCompletedForUserNoteById));
router.delete('/:id', asyncWrapper(deleteUserNoteById));

module.exports = router;
