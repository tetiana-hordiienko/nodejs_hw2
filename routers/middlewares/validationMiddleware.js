const Joi = require('joi');
const {BadRequestError} = require('../../models/errorModel');

module.exports.validateRegistration = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string()
        // .alphanum()
        // .min(3)
        // .max(30)
        .required(),
    password: Joi.string()
        .required()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await validateSchema(schema, req);
  next();
};

module.exports.validateCreationNote = async (req, res, next) => {
  const schema = Joi.object({
    text: Joi.string()
        .min(1)
        .required(),
  });

  await validateSchema(schema, req);
  next();
};

module.exports.validateChangePassword = async (req, res, next) => {
  const schema = Joi.object({
    oldPassword: Joi.string()
        .required()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
    newPassword: Joi.string()
        .required()
        .pattern(new RegExp('^[a-zA-Z0-9]{6,30}$')),
  });

  await validateSchema(schema, req);
  next();
};


// eslint-disable-next-line require-jsdoc
async function validateSchema(schema, req) {
  try {
    await schema.validateAsync(req.body);
  } catch (e) {
    throw new BadRequestError(e.message);
  }
}
